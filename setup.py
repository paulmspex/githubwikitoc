from setuptools import setup, find_packages
import pathlib

here = pathlib.Path(__file__).parent.resolve()

# Get the long description from the README file
long_description = (here / "README.md").read_text(encoding="utf-8")

setup(
    name='githubwikitoc',
    version='0.1.2',
    package_dir={"": "src"},
    packages=find_packages(where="src"),
    url='https://github.com/paulsuh/githubwikitoc',
    license='BSD 3-clause',
    author='Paul Suh',
    author_email='paul@mspex.net',
    description='This package generates a Table of Contents for a wiki on GitHub. ',
    long_description=long_description,
    long_description_content_type="text/markdown"
)
