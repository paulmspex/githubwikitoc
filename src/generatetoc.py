import sys
import argparse
from os import chdir
from src import githubwikitoc

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="Generate a Table of Contents for a GitHub Wiki. ")
    parser.add_argument("wiki_dir", help="Path to your git cloned wiki")
    parser.add_argument("-a", "--autoinsert",
                        help="Automatically insert the ToC into your Home.md file",
                        action="store_true")
    args = parser.parse_args()

    chdir( args.wiki_dir )

    if args.autoinsert:
        print("Generating ToC and inserting into Home.md")
        githubwikitoc.generate_toc()
    else:
        print("Generating ToC and printing to stdout")
        print(githubwikitoc.scan_files())
