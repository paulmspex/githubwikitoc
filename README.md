# GitHub Wiki TOC

This package will generate a table of contents for your GitHub Wiki, and
optionally insert it into the Home.md file. It can also be set up to 
auto-update the Home.md file when the wiki is edited. 

* Version: 0.1
* License: BSD 3-clause

## Installation

* Install the module from PyPi: `pip install githubwikitoc`

## Running the code

There are two modes of running the code: 

1) Generate the ToC and print it to stdout. You can then paste this 
   into your Home.md. 
2) Have the ToC automatically inserted into your Home.md. 

To start, clone your GitHub repo's wiki to your local file system: 

```shell
git clone https://github.com/paulsuh/<reponame>.wiki.git
```

To print the ToC to stdout: 

```shell
python generatetoc.py <path to cloned wiki>
```

To insert the code into your Home.md file: 

```shell
python generatetoc.py --autoinsert 
```